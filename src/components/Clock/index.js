import React, { Component } from 'react';

import { TimeBox, Data, Hora } from './styles';

export default class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date(),
    };
  }

  componentDidMount() {
    this.intervalID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  tick() {
    this.setState({
      time: new Date(),
    });
  }

  render() {
    const { time } = this.state;

    return (
      <TimeBox>
        <Data>{time.toLocaleDateString()}</Data>
        <Hora>{time.toLocaleTimeString()}</Hora>
      </TimeBox>
    );
  }
}
