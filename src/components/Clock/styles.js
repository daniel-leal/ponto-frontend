import styled from 'styled-components';

import colors from '~/styles/colors';

export const TimeBox = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${colors.red};
  color: ${colors.second};
  border-radius: 5px;
  margin: 0px 7px 0px;
  padding: 5px 50px;
`;

export const Data = styled.span`
  font-size: 1.5rem;
`;

export const Hora = styled.span`
  font-size: 2.5rem;
  font-weight: bold;
`;
