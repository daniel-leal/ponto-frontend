import React, { useEffect, useRef } from 'react';
import { useField } from '@unform/core';

export default function Input({ name, inputRef, ...rest }) {
  const reference = useRef(null);

  const { fieldName, defaultValue = '', registerField } = useField(name);
  useEffect(() => {
    registerField({
      name: fieldName,
      ref: reference.current,
      path: 'value',
    });
  }, [fieldName, registerField]);
  return (
    <>
      <input ref={reference} defaultValue={defaultValue} {...rest} />
    </>
  );
}
