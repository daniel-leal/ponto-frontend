import React from 'react';
import { Search } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { Container, Message } from './styles';

export default function NoResultFound() {
  return (
    <Container>
      <Search fontSize="large" />
      <Message>Nenhum registro encontrado.</Message>
      <Link to="/">
        <Button variant="contained" color="primary">
          Voltar
        </Button>
      </Link>
    </Container>
  );
}
