import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: auto;
  color: #fff;
`;

export const Message = styled.p`
  font-size: 1.8rem;
  padding: 0px 15px;
`;
