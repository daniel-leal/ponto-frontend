import React, { useRef, useState } from 'react';
import Swal from 'sweetalert2';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { Form } from '@unform/web';
import * as Yup from 'yup';

// Material
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Clock from '~/components/Clock';
import Input from '~/components/Input';
import { Container, LoginBox, Error, Global } from './styles';

import logo from '~/assets/banpara-logo.png';
import api from '~/services/api';

export default function Checkin() {
  const history = useHistory();
  const formRef = useRef(null);
  const [loginError, setLoginError] = useState(false);
  const [senhaError, setSenhaError] = useState(false);
  const [loginErrorMsg, setLoginErrorMsg] = useState('');
  const [senhaErrorMsg, setSenhaErrorMsg] = useState('');

  function getFieldErrors() {
    setLoginError(!!formRef.current.getFieldError('loginAD'));
    setLoginErrorMsg(formRef.current.getFieldError('loginAD'));
    setSenhaError(!!formRef.current.getFieldError('senha'));
    setSenhaErrorMsg(formRef.current.getFieldError('senha'));
  }

  async function validateForm() {
    try {
      // Get all data
      const allData = formRef.current.getData();

      // Remove all previous errors
      formRef.current.setErrors({});

      const schema = Yup.object().shape({
        loginAD: Yup.string().required('Campo obrigatório *'),
        senha: Yup.string()
          .min(6, 'Deve conter no mínimo, 6 caracteres')
          .required('Campo obrigatório *'),
      });

      await schema.validate(allData, {
        abortEarly: false,
      });
    } catch (err) {
      const validationErrors = {};

      if (err instanceof Yup.ValidationError) {
        err.inner.forEach((error) => {
          validationErrors[error.path] = error.message;
        });
        formRef.current.setErrors(validationErrors);
        getFieldErrors();
      } else {
        toast.error('Ocorreu um erro inesperado!');
      }
    }
  }

  async function handleSubmit(data) {
    try {
      await validateForm();

      // Validation passed
      await api.post('/bater-ponto', data);

      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Batida Registrada com sucesso!',
        html: new Date().toLocaleString(),
        showConfirmButton: false,
        timer: 5000,
      });
    } catch (err) {
      toast.error('Ocorreu um erro inesperado!');
    }
  }

  async function handleRecords() {
    try {
      // Get all data
      const allData = formRef.current.getData();

      await validateForm();

      const { data: records } = await api.post('/demonstrativo', allData);

      history.push({ pathname: '/demonstrativo', state: records });
    } catch (err) {
      toast.error('Ocorreu um erro inesperado!');
    }
  }

  return (
    <Container>
      <LoginBox>
        <header>
          <img src={logo} alt="banpara" className="logo" />
          <div className="titulo-sistema">
            <span className="titulo">Sistema de Ponto</span>
          </div>
        </header>

        <Form ref={formRef} noValidate onSubmit={handleSubmit}>
          <TextField
            InputProps={{
              inputComponent: Input,
            }}
            label="Login"
            name="loginAD"
            fullWidth
            type="email"
            error={loginError}
          />
          {loginError && <Error>{loginErrorMsg}</Error>}

          <TextField
            InputProps={{
              inputComponent: Input,
            }}
            label="Senha"
            name="senha"
            fullWidth
            type="password"
            error={senhaError}
          />
          {senhaError && <Error>{senhaErrorMsg}</Error>}

          <div className="actions">
            <Button variant="contained" onClick={handleRecords}>
              Demonstrativo
            </Button>

            <Button type="submit" variant="contained" color="primary">
              Bater Ponto
            </Button>
          </div>
        </Form>
      </LoginBox>

      <Clock />
    </Container>
  );
}
