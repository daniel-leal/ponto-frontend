import styled from 'styled-components';

import colors from '~/styles/colors';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  height: 60vh;
  text-align: center;

  form {
    display: flex;
    flex-direction: column;

    .actions {
      margin-top: 30px;
      display: flex;
      justify-content: space-around;
    }
  }
`;

export const LoginBox = styled.div`
  padding: 40px;
  min-width: 25%;
  background-color: #fff;
  border: 1px solid #bbdefb;
  border-radius: 4px;
  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14),
    0 1px 5px 0 rgba(0, 0, 0, 0.12);

  header {
    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    padding-bottom: 30px;

    .logo {
      width: 120px;
    }

    .titulo-sistema {
      font-weight: bold;
      align-items: center;
      margin: 0;
      font-size: 1rem;
      color: #ea1c24;
      text-transform: uppercase;
      text-align: right;
      line-height: 2em;
    }
  }
`;

export const Error = styled.span`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  color: ${colors.red};
  padding: 0px 5px;
`;
