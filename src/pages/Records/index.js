import React from 'react';
import { Link, useLocation } from 'react-router-dom';

// Material
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
} from '@material-ui/core';
import { Lock, TimerOff, Timer } from '@material-ui/icons';

import NoResultFound from '~/components/NoResultFound';
import { Container, Footer, Info } from './styles';
import colors from '~/styles/colors';

export default function Records() {
  const location = useLocation();
  const records = location.state;

  return (
    <Container>
      {records.length === 0 ? (
        <NoResultFound />
      ) : (
        <TableContainer className="table" component={Paper}>
          <header>
            <div className="login">
              {records[0].idFuncionario} - {records[0].loginAD}
            </div>
            <div className="lotacao">
              <Lock color="primary" /> #MATRIZ / DITEC / GERIN / CHEFIA
            </div>
            <p>
              <span>Carga horária:</span> 06 horas
            </p>
            <p>
              <span>Duração Desbloqueio:</span> 00h
            </p>
            <p>
              <span>Tempo máximo de intervalo:</span> 15 minutos
            </p>
          </header>

          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: 'bold', color: colors.label }}>
                  Data
                </TableCell>
                <TableCell
                  style={{ fontWeight: 'bold', color: colors.label }}
                  align="right"
                >
                  Hora
                </TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {records.map((record) => (
                <TableRow hover key={record.id}>
                  <TableCell component="th" scope="row">
                    {new Date(record.dataHora).toLocaleDateString()}
                  </TableCell>
                  <TableCell align="right">
                    {new Date(record.dataHora).toLocaleTimeString()}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

          <Info>
            <p>
              <Timer color="primary" />
              Tempo de trabalho parcial:&nbsp; <span>05:44:02</span>
            </p>
            <p>
              <TimerOff color="primary" />
              Fim da jornada atual: &nbsp;<span>14:18:49</span>
            </p>

            <hr />
          </Info>

          <Footer>
            <Link to="/">
              <Button variant="contained">Voltar</Button>
            </Link>
          </Footer>
        </TableContainer>
      )}
    </Container>
  );
}
