import styled from 'styled-components';

import colors from '~/styles/colors';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;

  .table {
    width: 50%;
  }

  header {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 20px 15px;

    .login {
      color: #1a237e;
      font-size: 1.2rem;
      font-weight: bold;
      padding-bottom: 5px;
    }

    .lotacao {
      display: flex;
      align-items: center;
      text-transform: uppercase;
      font-style: italic;
      color: ${colors.label};
      padding-bottom: 15px;
    }

    p {
      font-size: 0.9rem;
      color: ${colors.label};
      padding: 5px;

      span {
        font-weight: bold;
      }
    }
  }

  hr {
    padding-left: 5px;
  }
`;

export const Info = styled.div`
  flex: 1;
  flex-direction: column;
  padding: 50px 20px;

  p {
    display: flex;
    justify-content: flex-end;
    font-size: 1.1rem;
    color: ${colors.label};
    padding-bottom: 15px;
    color: ${colors.strong};

    span {
      font-weight: bold;
    }
  }
`;

export const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 15px;
`;
