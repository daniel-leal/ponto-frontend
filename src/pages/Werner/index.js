import React from 'react';

import { FaLock } from 'react-icons/fa';

// Bootstrap
import { Container, Row, Col } from 'react-bootstrap';

// Local Styles
import {
  CustomContainer,
  Logo,
  Sistema,
  Funcionario,
  Lotacao,
  Status,
} from './styles';
import colors from '~/styles/colors';

import logobranca2 from '~/assets/logobranca2.png';

const Werner = () => (
  <CustomContainer fluid>
    <Container className="pt-5 pb-2">
      <Row>
        <Col lg={6}>
          <Col lg={3} className="p-0">
            <Logo src={logobranca2} alt="logo" />
          </Col>
        </Col>
        <Col lg={6} className="text-right">
          <Sistema>REP Virtual</Sistema>
        </Col>
      </Row>
    </Container>

    <Container className="pb-5 pt-3">
      <Row>
        <Col lg={6}>
          <Funcionario>Werner Morhy Terrazas Filho</Funcionario>
          <br />
          <Lotacao>MATRIZ/DITEC/SUSIS/GERIN/CHEFIA</Lotacao>
          <br />
          <FaLock style={{ color: `${colors.red}` }} />{' '}
          <Status>Lotação Bloqueada</Status>
          <br />
        </Col>
      </Row>
    </Container>
  </CustomContainer>
);

export default Werner;
