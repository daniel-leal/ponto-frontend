import styled from 'styled-components';

import { Container, Badge } from 'react-bootstrap';

import colors from '~/styles/colors';

export const CustomContainer = styled(Container)`
  background-color: ${colors.primary};
  padding: 0;
  color: ${colors.white};
  border-bottom: ${colors.red} solid 6px;
`;

export const Logo = styled.img`
  max-width: 100%;
  height: auto;
  vertical-align: middle;
  border-style: none;
`;

export const Sistema = styled.span`
  color: #fff;
  padding-left: 0.5rem !important;
  padding-bottom: 0.5rem !important;

  padding-right: 0.5rem !important;
  padding-top: 0.25rem !important;
  border-color: #f8f9fa !important;
  border: 1px solid #dee2e6 !important;
`;

export const Funcionario = styled.span`
  font-family: 'Nunito', sans-serif !important;
  font-weight: 200;
  font-size: 32px;
`;

export const Lotacao = styled.span`
  font-family: 'Nunito', sans-serif !important;
  font-weight: 200;
  font-size: 14px;
  letter-spacing: 1.3px;
`;

export const Status = styled(Badge)`
  font-family: 'Nunito', sans-serif !important;
  background-color: ${colors.red};
  border-radius: 2px;
  font-weight: 400;
  letter-spacing: 1.1px;
  padding-top: 0.25rem !important;
  padding-right: 0.5rem !important;
`;
