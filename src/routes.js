import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from './services/history';

// Pages
import Checkin from '~/pages/Checkin';
import Records from '~/pages/Records';
import Werner from '~/pages/Werner';

export default function Routes() {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Werner} />
        <Route path="/demonstrativo" exact component={Records} />
      </Switch>
    </Router>
  );
}
