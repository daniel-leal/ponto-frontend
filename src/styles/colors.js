export default {
  primary: '#3f32ab',
  second: '#dddddd',
  dark: '#191920',
  white: '#fff',

  red: '#ff5b5b ',

  edit: '#4D85EE',
  delete: '#DE3B3B',

  linkActive: '#191920',
  disabled: '#dddddd',

  focus: '#edff00',
  bg: '#ebebeb',
  border: '#dddddd',
  placeholder: '#999',
  input: '#333',
  label: '#444444',
  strong: '#333',

  tableTd: '#666666',
  tableBorder: '#EEEEEE',
};
