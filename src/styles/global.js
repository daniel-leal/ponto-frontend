import { createGlobalStyle } from 'styled-components';
import 'react-perfect-scrollbar/dist/css/styles.css';
import 'react-toastify/dist/ReactToastify.css';

export default createGlobalStyle`
  /*Classes globais*/

* {
    margin:0;
    padding: 0;
}

img {
    max-width: 100%;
    height: auto;
  }

.nunito{
    font-family: 'Nunito', sans-serif !important;
}

.roboto{
    font-family: 'Roboto', sans-serif;
}

.montserrat{
    font-family: 'Montserrat', sans-serif;
}

hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

.vermelho {
    border: 1px solid red;
}

.fw-100 {
    font-weight: 100;
}

.fw-200 {
    font-weight: 200;
}

.fw-300 {
    font-weight: 300;
}

.fw-400 {
    font-weight: 400;
}

.fw-600 {
    font-weight: 600;
}

.fw-700 {
    font-weight: 700;
}

.fw-800 {
    font-weight: 800;
}

.fw-900 {
    font-weight: 900;
}



.font-12 {
    font-size: 12px;
}

.font-13 {
    font-size: 13px;
}

.font-14 {
    font-size: 14px;
}

.font-16 {
    font-size: 16px;
}

.font-18 {
    font-size: 18px;
}

.font-20 {
    font-size: 20px;
}

.font-22 {
    font-size: 22px;
}

.font-24 {
    font-size: 24px;
}

.font-28 {
    font-size: 28px;
}

.font-30 {
    font-size: 30px;
}

.font-32 {
    font-size: 32px;
}

/* Demais classes */

.sombra{
    -webkit-box-shadow: 1px 1px 10px 0px rgba(0,0,0,0.23);
    -moz-box-shadow: 1px 1px 10px 0px rgba(0,0,0,0.23);
    box-shadow: 1px 1px 10px 0px rgba(0,0,0,0.23);
}


.sombra2{
    -webkit-box-shadow: 1px 1px 4px 1px rgba(0,0,0,0.05);
    -moz-box-shadow: 1px 1px 4px 1px rgba(0,0,0,0.05);
    box-shadow: 1px 1px 4px 1px rgba(0,0,0,0.05);
}

.sombra3{
    -webkit-box-shadow: 0px 0px 15px -8px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 0px 15px -8px rgba(0,0,0,0.3);
    box-shadow: 0px 0px 15px -8px rgba(0,0,0,0.3);
}

.sombra4{
    -webkit-box-shadow: inset 0px -7px 30px 1px rgba(209,217,230,0.39);
-moz-box-shadow: inset 0px -7px 30px 1px rgba(209,217,230,0.39);
box-shadow: inset 0px -7px 30px 1px rgba(209,217,230,0.39);
}

.hora-ponto{
    font-size: 30px;
    font-weight: 300;
    display: block;
    line-height: 1;
}

.data-ponto{
    font-size: 14px;
    font-weight: 300;
    line-height: 0.5;
}

.topo{
    padding: 0;
    color: #ffffff;
    border-bottom: #ff5b5b solid 6px;
}

.cinza{
    background-color: #f1f4f9;
}

.cartao{
    background-color: white;
}

.cartao2{
    border: none;
}


.subtitulocartao{
    font-size: 13px;
    color: black;
    letter-spacing: 0.5px;
}

.hora-ponto{
    font-size: 30px;
    color: black;
    letter-spacing: 0.5px;
}

.hora-ponto2{
    font-size: 22px;
    color: black;
    letter-spacing: 0.5px;
}

.hora-ponto3{
    font-size: 26px;
    color: black;
    letter-spacing: 1.5px;
}

.hora-ponto4{
    font-size: 26px;
    color: black;
    letter-spacing: 1.5px;
    font-weight: 200;
}

.tempo-real{
    color: white;
    border-radius:20px 0px 0px 0px;
    background: rgba(2,42,112,1);
    background: -moz-linear-gradient(45deg, rgba(2,42,112,1) 0%, rgba(2,42,112,1) 12%, rgba(0,66,158,1) 36%, rgba(2,98,216,1) 70%, rgba(2,98,216,1) 81%, rgba(2,99,216,1) 100%);
    background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(2,42,112,1)), color-stop(12%, rgba(2,42,112,1)), color-stop(36%, rgba(0,66,158,1)), color-stop(70%, rgba(2,98,216,1)), color-stop(81%, rgba(2,98,216,1)), color-stop(100%, rgba(2,99,216,1)));
    background: -webkit-linear-gradient(45deg, rgba(2,42,112,1) 0%, rgba(2,42,112,1) 12%, rgba(0,66,158,1) 36%, rgba(2,98,216,1) 70%, rgba(2,98,216,1) 81%, rgba(2,99,216,1) 100%);
    background: -o-linear-gradient(45deg, rgba(2,42,112,1) 0%, rgba(2,42,112,1) 12%, rgba(0,66,158,1) 36%, rgba(2,98,216,1) 70%, rgba(2,98,216,1) 81%, rgba(2,99,216,1) 100%);
    background: -ms-linear-gradient(45deg, rgba(2,42,112,1) 0%, rgba(2,42,112,1) 12%, rgba(0,66,158,1) 36%, rgba(2,98,216,1) 70%, rgba(2,98,216,1) 81%, rgba(2,99,216,1) 100%);
    background: linear-gradient(45deg, rgba(2,42,112,1) 0%, rgba(2,42,112,1) 12%, rgba(0,66,158,1) 36%, rgba(2,98,216,1) 70%, rgba(2,98,216,1) 81%, rgba(2,99,216,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#022a70', endColorstr='#0263d8', GradientType=1 );
}

.tempo-real2{
    color: white;
    border-radius:20px 0px 0px 0px;
    background: rgba(255,81,99,1);
    background: -moz-linear-gradient(45deg, rgba(255,81,99,1) 0%, rgba(255,81,99,1) 12%, rgba(255,81,99,1) 36%, rgba(248,73,91,1) 63%, rgba(248,73,91,1) 81%, rgba(248,73,91,1) 100%);
    background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(255,81,99,1)), color-stop(12%, rgba(255,81,99,1)), color-stop(36%, rgba(255,81,99,1)), color-stop(63%, rgba(248,73,91,1)), color-stop(81%, rgba(248,73,91,1)), color-stop(100%, rgba(248,73,91,1)));
    background: -webkit-linear-gradient(45deg, rgba(255,81,99,1) 0%, rgba(255,81,99,1) 12%, rgba(255,81,99,1) 36%, rgba(248,73,91,1) 63%, rgba(248,73,91,1) 81%, rgba(248,73,91,1) 100%);
    background: -o-linear-gradient(45deg, rgba(255,81,99,1) 0%, rgba(255,81,99,1) 12%, rgba(255,81,99,1) 36%, rgba(248,73,91,1) 63%, rgba(248,73,91,1) 81%, rgba(248,73,91,1) 100%);
    background: -ms-linear-gradient(45deg, rgba(255,81,99,1) 0%, rgba(255,81,99,1) 12%, rgba(255,81,99,1) 36%, rgba(248,73,91,1) 63%, rgba(248,73,91,1) 81%, rgba(248,73,91,1) 100%);
    background: linear-gradient(45deg, rgba(255,81,99,1) 0%, rgba(255,81,99,1) 12%, rgba(255,81,99,1) 36%, rgba(248,73,91,1) 63%, rgba(248,73,91,1) 81%, rgba(248,73,91,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5163', endColorstr='#f8495b', GradientType=1 );
}

.etiqueta{
    border-radius: 2px;
    font-weight: 400;
    letter-spacing: 1.1px;
}

.etiqueta-azul{
    font-size: 10px;
    font-weight: 100px;
    border-radius: 2px;
    color:#35b8e0;
    background-color:rgba(53,184,224,.40)
}

.etiqueta-vermelha{
    font-size: 10px;
    font-weight: 100px;
    border-radius: 2px;
    color:#ff5b5b;
    background-color:rgba(255,91,91,.30)
}


.campoform{
    height: 35px;
}

.botao{
    font-size: 15px;
    font-weight: 400;
    border-radius: 2px;
    font-family: 'Nunito', sans-serif !important;
}

.tabela{
    border-radius: 6px;
}

.font-space{
    letter-spacing: 1.3px;
}
`;
